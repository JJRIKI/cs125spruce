cs125spruce repository
James Bek, Nathan Young, Kevin Gao, Rebecca Frink

Bookends files for group spruce

created at Phase2
Hosted on VMWARDROBE

CreateSchema.sql is responsible for creating the tables for relational schemas and converting all of the relational schemas in our E/R diagram into MySQL tables.

DropSchema.sql is for dropping all the tables.

PopulateSmall.sql is responsible for populating a small amount of data/ tuples into our database.

PopulateLarge.sql is responsible for populating a large amount of data/tuples into our database.

The point of making PopulateSmall.sql and PopluateLarge.sql is to test whether are able to write queries into our database and make sure it is functioning. 

As far as collaboration, 
All team collaborated on the E/R diagram.
Rebecca did PopulateSmall.sql
James did CreateSchema.sql
Rebecca and James collaborated on DropSchema.sql
Nathan Young did PopluateLarge.sql
Kevin Gao did the readme.md

Phase3

new files are as follows:

a.sql has 5 non-simple queries to show some typical uses that the BookEnds clients will use our database for. This file was created by the entire team.

b.sql has 5 different data-modification queries of varying complexity. Created by James, Kevin, and Rebecca.

c.sql creates 2 different views and shows off 2 queries that utilize the newly created views. Created by James and Nathan.

d.sql creates 2 indexes and tests their impact on the queries they affect. Created by Nathan and Rebecca.

We have since made updates to our createschema.sql and populatelarge.sql, which has created some bugs amongst these previous files. We will fix these problems soon.

Phase4

added new file triggerfunctions.sql, which includes a PostgreSQL function (created by Rebecca and Nathan) and trigger (created by Keving and James), along with queries to prove their use.

updates were made to the following files:

createschema.sql was rewritten in PostgreSQL, given primary/foreign keys on the attributes that needed them, and added 3 checks on various tables to enforce more constraints.
Kevin, Rebecca, and Nathan completed checks while James did primary/foreign keys.

populatelarge.sql was rewritten in PostgreSQL. Updated by Nathan'

dropschema.sql was rewritten in PostgreSQL. Updated by James.






