<!DOCTYPE html>

<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>BookEnds Donations</title>

		<link rel="stylesheet" href="app.css">
	</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="homepage.php">BookEnds</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="about.html">About</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="donate.php">Donate</a>
				</li>
			</ul>

			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="myacc.html">My Account</a>
				</li>
			</ul>

			<button class="btn btn-outline-light my-2 my-sm-0" type="submit">Log Out</button>

		</div>
    </nav> 
    <div class="container">
    	<form action="insert.php" method="POST">
    		<div class="row">
            	<div class="form-group">
               		<label for="donate_type">Donation Type</label>
               		<select class="form-control" id="donate_type">
                   		<option>Book Donation</option>
                   		<option>Money Donation</option>
               		</select>
       			</div>
        	</div>
        	<div class="row">
        		<div class="form-group">
        			<div class="row">
        				<div class="col">   	     			
        					<label for="book_title">Book Title</label>
        				</div>
        				<div class="col">
        					<input type="text" name="book_title">
        				</div>
        			</div>


        			<div class="row">
        				<div class="col">					
        					<label for="author">Author</label>
        				</div>
        				<div class="col">
							<input type="text" name="author">
						</div>
					</div>


					<div class="row">
						<div class="col">					
							<label for="publisher">Publisher</label>
						</div>
						<div class="col">
							<input type="text" name="publisher">
						</div>
					</div>


					<div class="row">
						<div class="col">					
							<label for="genre">Genre</label>
						</div>
						<div class="col">
							<input type="text" name="genre">
						</div>
					</div>

					<div class="row">
							<div class="col">
							<label for="work_title">Work Title</label>
						</div>
						<div class="col">
							<input type="text" name="work_title">
						</div>
					</div>
					
					<div class="row"><button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Submit</button></div>
					
        		</div>
        	</div>
    	</form>

    </div>    

</body>
</html>