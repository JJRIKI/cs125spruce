from django.db import models

# Create your models here.
from django.db import models
class Author(models.Model):
    fname = models.CharField(max_length=50)
    lname = models.CharField(max_length=50)
    role = models.CharField(max_length=50)

    def __unicode__(self):
        return self.fname+" "+self.lname
