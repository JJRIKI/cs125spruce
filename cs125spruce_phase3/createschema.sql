CREATE TABLE People (
        person_id int UNIQUE auto_increment,
        username varchar(80),
        password varchar(80),
        first_name varchar(80),
        last_name varchar(80),
        email varchar(80),
        phone_number double
);

CREATE TABLE Staff (
        staff_id int UNIQUE auto_increment,
        person_id int,
        role varchar(80)
);


CREATE TABLE Donor (
        donor_id int UNIQUE auto_increment,
        person_id int,
        donation_id int,
        recv_receipt BIT
);



CREATE TABLE Donation (
        donation_id int UNIQUE auto_increment,
        book_id int,
        date_donated date,
        donation_type varchar(80),
        amount_donated int
);



CREATE TABLE Client (
        client_id int UNIQUE auto_increment,
        request_id int,
        person_id int,
        child_id int, 
        how_found varchar(9000),
        LT_id int UNIQUE
);


CREATE TABLE Child (
        child_id int UNIQUE auto_increment,	
        birth_year year,
        name varchar(80),
        interests varchar(9000),
        reading_level varchar(80),
        last_update date
);


CREATE TABLE Request (
        request_id int UNIQUE auto_increment,
        num_books int,
        book_id int,
        request_date date,
        special_request varchar(9000)
);


CREATE TABLE Location (
        request_id int,
        delivery_method varchar(80),
        country varchar(80),
        address varchar(80)
);


CREATE TABLE Orders (
        order_id int UNIQUE auto_increment,
        request_id int,
        order_status varchar(80),
        order_stage varchar(80),
        date_shipped date,
        num_boxes int(9),
        box_size varchar(80),
        payment_status varchar(80),
	payment_total int,
        payment_paid int
);


CREATE TABLE Book (
        book_id int UNIQUE auto_increment,
        google_id int UNIQUE,
        LT_id int,
        title varchar(80),
        date_published date,
        edition varchar(80),
        book_status varchar(80),
        ISBN_10 varchar(10),
        ISBN_13 varchar(13)
);

CREATE TABLE Work (
        work_id int UNIQUE auto_increment,
        google_id int UNIQUE,
        book_id int,
        genre varchar(80),
        author varchar(80),
        publisher varchar(80),
        work_title varchar(80),
        LT_work_id int UNIQUE
);
