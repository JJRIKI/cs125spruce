

#this data modification command is designed for changing the book on the shelf for over a year into staled.
#author :Kevin Gao
#date:11/1/2019
UPDATE Book
SET book_status = 'staled'
WHERE  book_id IN ( SELECT date_donated FROM Donation  WHERE DATEDIFF( CURRENT_TIMESTAMP, date_donated) >=365 ORDER By title);

#This data modification is for the question how to find the most recently added books.
#author : Kevin Gao
#date: 11/1/2019
UPDATE Book
SET book_status = 'MostRecently_added'
WHERE  book_id IN (SELECT date_donated FROM Donation  WHERE not book_status ='shipped' AND DATEDIFF( CURRENT_TIMESTAMP,date_donated)<=60
ORDER By title);



#This updates the status of a book "shipped" by Orders that have been shipped
#Culprit: James Bek
UPDATE Book
SET book_status = 'shipped'
WHERE book_id IN (
	SELECT book_id
	FROM Request join Orders ON Request.request_id = Orders.request_id 
	WHERE order_status='shipped');

#This updates the status of an order to stalled if it goes over a certain amount of time
#Culprit: James Bek
UPDATE Orders
SET order_status = 'stalled'
WHERE order_stage = 'in process' AND Orders.request_id IN (SELECT request_id FROM Request WHERE DATEDIFF(CURRENT_TIMESTAMP, Request.request_date) > 7);

#Rebecca Frink
#This updates the status of the payment when the client has paid in full
UPDATE Orders
SET payment_status=1
WHERE payment_total - payment_paid = 0;


