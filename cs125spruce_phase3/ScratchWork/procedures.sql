CREATE Procedure Spruce1
As
SELECT book_id, title, author, request_id, email
FROM (Work join Book join Request join Client)
WHERE Book.book_status = 'Requested'
GO;

CREATE PROCEDURE Spruce2
AS
SELECT first_name, last_name, payment_total - payment_payed, date_shipped
FROM (Client join Orders)
WHERE order_status = 'Sent' AND payment_total <> payment_payed
GO;

CREATE PROCEDURE Spurce3
AS
SELECT country, num_books, Donor.first_name, Donor.last_name 
FROM (Location join Requst join Donor)
WHERE donor
