SELECT first_name, last_name, donor_id, Donor.donation_id, date_donated, recv_receipt
FROM People JOIN Donor on People.person_id = Donor.person_id JOIN Donation on Donor.donation_id = Donation.donation_id
WHERE recv_receipt = 0
LIMIT 10;

SELECT genre, title, author
FROM Book JOIN Work
WHERE book_status = 'Available'
ORDER BY author
LIMIT 10;

SELECT sub1.title 
FROM (SELECT genre, book_status, title, Book.book_id FROM Book JOIN Work ON Book.book_id=Work.book_id) as sub1 
	JOIN Request ON Request.book_id=sub1.book_id 
	JOIN Client ON Request.request_id=Client.request_id 
	JOIN Child ON Client.child_id=Child.child_id 
WHERE sub1.book_status = 'Sent' 
LIMIT 10;


SELECT first_name, last_name, payment_total - payment_paid 
FROM People JOIN Client ON Client.person_id = People.person_id 
	JOIN Request ON Request.request_id=Client.request_id
	JOIN Orders ON Orders.request_id=Request.request_id
WHERE payment_total-payment_paid <> 0 LIMIT 10;

#this code designed for find out all the old books on the shelf based on the datedifference
#author:kevin

SELECT book_status, date_donated
FROM Book join Donation
WHERE DATEDIFF(CURRENT_TIMESTAMP, date_donated) > 1 
ORDER By title
LIMIT 10;
