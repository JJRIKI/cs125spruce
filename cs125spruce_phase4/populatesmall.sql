﻿INSERT INTO People (person_id, username, password, first_name, last_name, email, phone_number) VALUES (1, "jbek", "password", "James", "Bek", "jbek@westmont.edu", 4087816056);
INSERT INTO People (person_id, username, password, first_name, last_name, email, phone_number) VALUES (2, "kegao", "password2", "Kevin", "Gao", "kegao@westmont.edu", 4087816057);

INSERT INTO Staff (staff_id, person_id, role) VALUES (1, 1, 'Developper');
INSERT INTO Staff (staff_id, person_id, role) VALUES (2, 2, 'Admin');

INSERT INTO Donor (donor_id, person_id, donation_id, recv_receipt) VALUES (1, 1, 43, 0);
INSERT INTO Donor (donor_id, person_id, donation_id, recv_receipt) VALUES (2, 2, 21, 1);

INSERT INTO Donation (donation_id, book_id, date_donated, donation_type, amount_donated) VALUES (43, 100, "2008-11-11", "Book", 1);
INSERT INTO Donation (donation_id, book_id, date_donated, donation_type, amount_donated) VALUES (45, 99, "2008-11-11", "Book", 1);

INSERT INTO Client (client_id, request_id, person_id, child_id, how_found, LT_id) VALUES (1, 40, 2, 1, "Rodkey recommended", 1212);
INSERT INTO Client (client_id, request_id, person_id, child_id, how_found, LT_id) VALUES (2, 41, 1, NULL, "Developer", 1211);

INSERT INTO Child (child_id, birth_year, name, interests, reading_level, last_update) VALUES (1, "2018", "Becky", "gaming, sports", "None", "2019-01-11");

INSERT INTO Request (request_id, num_books, book_id, request_date, special_request) VALUES (40, 1, 100, "2019-11-01", NULL);
INSERT INTO Request (request_id, num_books, book_id, request_date, special_request) VALUES (41, 1, 99, "2019-10-01", "Special Edition");

INSERT INTO Location (request_id, delivery_method, country, address) VALUES (40, "U.S. Postal Service", "United State of America", "address1");
INSERT INTO Location (request_id, delivery_method, country, address) VALUES (41, "Amazon", "United States of America", "address2");

INSERT INTO Orders (order_id, order_status, order_stage, date_shipped, num_boxes, box_size, payment_total, payment_paid) VALUES (1, "incomplete", "in process", NULL, 1, "small", 200, 100);
INSERT INTO Orders (order_id, order_status, order_stage, date_shipped, num_boxes, box_size, payment_total, payment_paid) VALUES (2, "shipped", "Finished", "2019-11-01", 1, "small", 100, 100);

INSERT INTO Book (book_id, google_id, LT_id, title, date_published, edition, book_status, ISBN_10, ISBN_13) VALUES (100, 69, 169, "Sadness", "1999-12-12", "1st", "available", 666, 665);
INSERT INTO Book (book_id, google_id, LT_id, title, date_published, edition, book_status, ISBN_10, ISBN_13) VALUES (99, 68, 168, "Depression", "1998-12-12", "2nd", "unavailable", 667, 668);

INSERT INTO Work (work_id, google_id, book_id, genre, author, publisher, work_title, LT_work_id) VALUES (88, 31, 100, "History", "John Doe", "Publisher1", "The human condition", 5555);
INSERT INTO Work (work_id, google_id, book_id, genre, author, publisher, work_title, LT_work_id) VALUES (87, 32, 99, "Psychological Horror", "John Smith", "Publisher2", "The human mind", 4444);
