--find all donors who have not recieved a receipt yet
SELECT first_name, last_name, date_donated, recv_receipt
FROM People JOIN Donor on People.person_id = Donor.person_id JOIN Donation on Donor.donor_id = Donation.donor_id
WHERE recv_receipt = 0
LIMIT 10;

--find all available books
SELECT genre, title, author
FROM Book JOIN Work ON Book.book_id = Work.book_id
WHERE book_status = 'Available'
ORDER BY author
LIMIT 10;

--find the send books and what kids got them
SELECT sub1.title, birth_year, reading_level 
FROM (SELECT genre, book_status, title, Book.request_id FROM Book JOIN Work ON Book.book_id=Work.book_id) as sub1 
	JOIN Request ON Request.request_id=sub1.request_id 
	JOIN Client ON Request.client_id=Client.client_id 
	JOIN Child ON Client.client_id=Child.client_id 
WHERE sub1.book_status = 'Sent' 
LIMIT 10;

--find the clients who have not paid for their order yet
SELECT first_name, last_name, payment_total - payment_paid 
FROM People JOIN Client ON Client.person_id = People.person_id 
	JOIN Request ON Request.client_id=Client.client_id
	JOIN Orders ON Orders.request_id=Request.request_id
WHERE payment_total-payment_paid > 0 LIMIT 10;

--this code designed for find out all the old books on the shelf based on the datedifference
--author:kevin

SELECT book_status, date_donated
FROM Book JOIN Donation ON Book.book_id = Donation.book_id
WHERE ((DATE_PART('year', current_date::date)) - (DATE_PART('year', date_donated::date))) > 1 
ORDER By title
LIMIT 10;
