CREATE TABLE People (
	person_id int UNIQUE auto_increment,
	username varchar(80),
	password varchar(80),
	first_name varchar(80),
	last_name varchar(80),
	email varchar(80),
	phone_number double,
	PRIMARY KEY (person_id)
);


CREATE TABLE Staff (
	staff_id int UNIQUE auto_increment,
	person_id int,
	role varchar(80),
	PRIMARY KEY (staff_id),
	FOREIGN KEY (person_id) REFERENCES People(person_id)

);


CREATE TABLE Donor (
	donor_id int UNIQUE auto_increment,
	person_id int,
	recv_receipt BIT,
	PRIMARY KEY (donor_id),
	FOREIGN KEY (person_id) REFERENCES People(person_id)
);


CREATE TABLE Donation (
        donation_id int UNIQUE auto_increment,
        donor_id int,
	book_id int,
	date_donated date,
        donation_type varchar(80),
        amount_donated int,
	PRIMARY KEY(donation_id),
	FOREIGN KEY (donor_id) REFERENCES Donor(donor_id),
	CHECK (date_donated <= CURRENT_TIMESTAMP)
);


CREATE TABLE Client (
	client_id int UNIQUE auto_increment,
	person_id int,
	how_found varchar(9000),
	LT_id int UNIQUE,
	PRIMARY KEY (client_id),
	FOREIGN KEY (person_id) REFERENCES People(person_id)
);


CREATE TABLE Child (
	child_id int UNIQUE auto_increment,
	client_id int,
	birth_year year,
	name varchar(80),
	interests varchar(9000),
	reading_level varchar(80),
	last_update date,
	PRIMARY KEY (child_id),
	FOREIGN KEY (client_id) REFERENCES Client(client_id)
);


CREATE TABLE Request (
	request_id int UNIQUE auto_increment,
	client_id int,
	num_books int,
	request_date date,
	special_request varchar(9000),
	PRIMARY KEY (request_id),
	FOREIGN key (client_id) REFERENCES Client(client_id)
);


CREATE TABLE Location (
	request_id int,
	delivery_method varchar(80),
	country varchar(80),
	address varchar(80),
	FOREIGN KEY (request_id) REFERENCES Request(request_id)
);


CREATE TABLE Orders (
	request_id int,
	order_status varchar(80),
	order_stage varchar(80),
	date_shipped date,
	num_boxes int(9),
	box_size varchar(80),
	payment_status varchar(80),
	payment_total int,
        payment_paid int,
	FOREIGN KEY (request_id) REFERENCES Request(request_id),
	CHECK (payment_paid >=0)
);


CREATE TABLE Book (
	book_id int UNIQUE auto_increment,
	google_id int UNIQUE,
	request_id int,
	LT_id int,
	title varchar(80),
	date_published date,
	edition varchar(80),
	book_status varchar(80),
	ISBN_10 varchar(10),
	ISBN_13 varchar(13),
	PRIMARY KEY (book_id),
	FOREIGN KEY (request_id) REFERENCES Request(request_id)
);


CREATE TABLE Work (
	work_id int UNIQUE auto_increment,
	google_id int UNIQUE,
	book_id int,
	genre varchar(80),
	author varchar(80),
	publisher varchar(80),
	work_title varchar(80),
	LT_work_id int UNIQUE,
	PRIMARY KEY (work_id)
);
