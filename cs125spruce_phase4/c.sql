--Creates a view that displays the people who owe money
CREATE VIEW Amount_Owed AS
SELECT Request.request_id, first_name, last_name, payment_status
FROM People JOIN Client ON People.person_id = Client.person_id JOIN Request ON Client.client_id = Request.client_id JOIN Orders ON Orders.request_id = Request.request_id
WHERE payment_status = '0';

SELECT * FROM Amount_Owed limit 10;

--A view for viewing the countries that donors books were donated to

CREATE VIEW BookDestinations AS
SELECT first_name, last_name, country 
FROM Location JOIN Request ON Location.request_id=Request.request_id JOIN Book ON Request.request_id=Book.request_id JOIN Donation ON Donation.book_id=Book.book_id JOIN Donor ON Donor.donor_id=Donation.donor_id JOIN People ON People.person_id=Donor.person_id
ORDER BY last_name;

SELECT * FROM BookDestinations WHERE country LIKE 'b%' LIMIT 10;
