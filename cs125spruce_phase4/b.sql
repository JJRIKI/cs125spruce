

--this data modification command is designed for changing the book on the shelf for over a year into staled.
--author :Kevin Gao
--date:11/1/2019
UPDATE Book
SET book_status = 'stale'
WHERE book_id IN (SELECT book_id 
	FROM Donation  
	WHERE (DATE_PART('year', current_date) -  DATE_PART('year', date_donated) >=1) 
	ORDER By title);

--This data modification is for the question how to find the most recently added books.
--author : Kevin Gao
--date: 11/1/2019
UPDATE Book
SET book_status = 'MostRecently_added'
WHERE book_id IN (SELECT book_id 
	FROM Donation  
	WHERE not book_status ='shipped' AND (DATE_PART('month', current_date) - DATE_PART('month',date_donated) <= 2)
	ORDER By title);



--This updates the status of a book "shipped" by Orders that have been shipped
--Culprit: James Bek
UPDATE Book
SET book_status = 'Sent'
WHERE request_id IN (
	SELECT Orders.request_id
	FROM Request join Orders ON Request.request_id = Orders.request_id
	WHERE order_status='shipped');

--This updates the status of an order to stalled if it goes over a certain amount of time
--Culprit: James Bek
UPDATE Orders
SET order_status = 'stalled'
WHERE order_stage = 'in process' AND request_id IN (SELECT Orders.request_id 
							FROM Orders 
							join Request on Orders.request_id = Request.request_id 
							WHERE ((DATE_PART('month', current_date) - DATE_PART('month', request_date))) > 1);



--Rebecca Frink
--This updates the status of the payment when the client has paid in full
UPDATE Orders
SET payment_status = '1'
WHERE payment_total - payment_paid = 0;


