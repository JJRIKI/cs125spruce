

--creates an index that is on the available books
--Rebecca Frink
CREATE INDEX availablebooks ON Book(book_status);
CREATE INDEX peopleinfo ON People(first_name, last_name, person_id);
