--We want to create a table book_requests where we hold the request_id and a list of book_ids, we use the tuple holding request_id and book_id list as arguments inside of the function
--We use those arguments to iterate through books and set the Book.request_id to the request_id in the argument.

--A function that when inputted with a book name, searches for the shelf location and surrounding books
CREATE OR REPLACE FUNCTION find_location (book_title VARCHAR(50))  
RETURNS 
text AS
$search$  
DECLARE  
	search_book RECORD; 
	cur_books CURSOR 
		FOR SELECT Book.title, Work.genre 
		FROM Book JOIN Work on Book.book_id=Work.book_id 
		WHERE Book.book_status<>'Sent' 
		AND Work.genre IN 
			(SELECT Work.genre
			 FROM Book JOIN Work on Book.book_id=Work.book_id 
			 WHERE Book.title=book_title 
			 LIMIT 1)
		ORDER BY title;
	value varchar DEFAULT '';
	shelf VARCHAR(50);
BEGIN  
OPEN cur_books;
LOOP
	FETCH cur_books INTO search_book;
	EXIT WHEN NOT FOUND;
	IF search_book.title > book_title THEN
		MOVE RELATIVE -4 FROM cur_books;
		FETCH cur_books INTO search_book;
		value:=value||'  '||search_book.title;

		FETCH cur_books INTO search_book;
		value:=value||'  '||search_book.title;

		FETCH cur_books INTO search_book;
		value:=value||'  '||search_book.title;
		shelf:=search_book.genre;

		FETCH cur_books INTO search_book;
		value:=value||'  '||search_book.title;

		FETCH cur_books INTO search_book;
		value:=value||'  '||search_book.title;
		
		EXIT;
		
	END IF;
END LOOP;
RETURN 'The book is on shelf '||shelf||' with these books: '||value;
END;  
$search$ 
LANGUAGE plpgsql;
 
--query to show function at work
SELECT find_location('tfpvp');



--function to access a request made and reserve the books for the request
CREATE OR REPLACE FUNCTION make_request()
RETURNS TRIGGER AS $total$
DECLARE
        r_id integer = NEW.request_id;
        new_bl integer array = NEW.book_id_list;
	x integer;
BEGIN
	for x IN 1 .. array_upper(new_bl, 1) LOOP
		UPDATE Book
		SET book_status = 'Requested', request_id = r_id
		WHERE book_status = 'Available' AND book_id = new_bl[x];
	END LOOP;
return NEW;
END; $total$ LANGUAGE plpgsql;
--trigger that activates function make_request() upon insert on requested_books
CREATE TRIGGER recv_request AFTER INSERT
ON requested_books
FOR EACH ROW
EXECUTE PROCEDURE make_request();


--insertions to test make_request()
SELECT * FROM requested_books;

UPDATE book
SET request_id = NULL, book_status = 'Available'
WHERE book_id = 18 OR book_id = 6462 OR book_id = 3660 OR book_id = 4540;

SELECT * FROM book WHERE book_id = 18 OR book_id = 6462 OR book_id = 3660 OR book_id = 4540;

insert into requested_books values (395, '{18, 6462}');
insert into requested_books values (2001, '{3660, 4540}');

SELECT * FROM requested_books;

SELECT * FROM book WHERE book_id = 18 OR book_id = 6462 OR book_id = 3660 OR book_id = 4540;
