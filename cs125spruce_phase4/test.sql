\echo DROPFUNCTIONS

\echo

drop function if exists make_request() cascade;
drop function if exists find_location() cascade;

\echo

\echo DROPSCHEMA.SQL
\i dropschema.sql

\echo

\echo CREATESCHEMA.SQL
\i createschema.sql

\echo

\echo POPULATELARGE.SQL
\i populatelarge.sql

\echo

\echo TRIGGERFUNCTIONS&TESTS
\i triggerfunctions.sql

\echo

\echo A.SQL
\i a.sql

\echo

\echo B.SQL
\i b.sql

\echo

\echo C.SQL
\i c.sql

\echo

\echo D.SQL
\i d.sql
