CREATE TABLE People (
	
	person_id serial PRIMARY KEY,
	username varchar(80),
	password varchar(80),
	first_name varchar(80),
	last_name varchar(80),
	email varchar(80),
	phone_number int
);


CREATE TABLE Staff (
	staff_id serial PRIMARY KEY,
	person_id serial REFERENCES People(person_id),
	role varchar(80)
);


CREATE TABLE Donor (
	donor_id serial PRIMARY KEY,
	person_id int,
	recv_receipt int
);



CREATE TABLE Client (
	client_id serial PRIMARY KEY,
	person_id serial REFERENCES People(person_id),
	how_found varchar(9000),
	LT_id int UNIQUE
);


CREATE TABLE Child (
	child_id serial PRIMARY KEY,
	client_id serial REFERENCES Client(client_id),
	birth_year varchar(80),
	name varchar(80),
	interests varchar(9000),
	reading_level varchar(80),
	last_update date
);


CREATE TABLE Request (
	request_id serial PRIMARY KEY,
	client_id serial REFERENCES Client(client_id),
	num_books int, CHECK (num_books>= 0),
	request_date date,
	special_request varchar(9000)
);


CREATE TABLE Location (
	request_id serial REFERENCES Request(request_id),
	delivery_method varchar(80),
	country varchar(80),
	address varchar(80)
);


CREATE TABLE Orders (
	request_id serial REFERENCES Request(request_id),
	order_status varchar(80),
	order_stage varchar(80),
	date_shipped date,
	num_boxes int,
	box_size varchar(80),
	payment_status varchar(80),
	payment_total int,
    payment_paid int,
    CHECK (payment_paid >=0)
);


CREATE TABLE Book (
	book_id serial PRIMARY KEY,
	google_id int UNIQUE,
	request_id int REFERENCES Request(request_id),
	LT_id int,
	title varchar(80),
	date_published date,
	edition varchar(80),
	book_status varchar(80),
	ISBN_10 varchar(10),
	ISBN_13 varchar(13)
);


CREATE TABLE Donation (
	donation_id serial PRIMARY KEY,
	donor_id serial REFERENCES Donor(donor_id),
	book_id serial REFERENCES Book(book_id),
	date_donated date,
	donation_type varchar(80),
	amount_donated int, CHECK ((amount_donated > 0 OR amount_donated = 0) AND (date_donated <= CURRENT_TIMESTAMP))
);

CREATE TABLE Work (
	work_id serial PRIMARY KEY,
	google_id int UNIQUE,
	book_id int,
	genre varchar(80),
	author varchar(80),
	publisher varchar(80),
	work_title varchar(80),
	LT_work_id int UNIQUE
);

CREATE TABLE requested_books (
	request_id SERIAL REFERENCES Request(request_id),
	book_id_list int ARRAY
);
